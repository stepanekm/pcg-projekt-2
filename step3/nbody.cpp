/**
 * @file      nbody.cpp
 *
 * @author    Martin Štěpánek \n
 *            Faculty of Information Technology \n
 *            Brno University of Technology \n
 *            xstepa59@stud.fit.vutbr.cz
 *
 * @brief     PCG Assignment 2
 *            N-Body simulation in ACC
 *
 * @version   2021
 *
 * @date      11 November  2020, 11:22 (created) \n
 * @date      11 November  2020, 11:37 (revised) \n
 *
 */

#include <math.h>
#include <cfloat>
#include "nbody.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                       Declare following structs / classes                                          //
//                                  If necessary, add your own classes / routines                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Swap two Particles objects
void swap(Particles& rhs, Particles& lhs)
{
  // Swap CPU internals of object
  std::swap(rhs.n, lhs.n);
  std::swap(rhs.vel, lhs.vel);
  std::swap(rhs.posW, lhs.posW);
  //Swap GPU internals of objects in ACC block
  #pragma acc serial present(rhs, lhs)
  {
    // For some reason, std::swap doesn't work here.
    // Swap variables in good old way by using temporary variable
    float4* tmp = rhs.posW;
    rhs.posW = lhs.posW;
    lhs.posW = tmp;
    tmp = rhs.vel;
    rhs.vel = lhs.vel;
    lhs.vel = tmp;
    int tmp_n = rhs.n;
    rhs.n = lhs.n;
    lhs.n = tmp_n;
  }
}

/// Compute gravitation velocity
void calculate_velocity(const Particles& p1,
                                    Particles&       p2,
                                    const int        N,
                                    const float      dt)
{
  #pragma acc parallel loop vector_length(512) present(p1, p2)
  for(int i = 0; i < N; ++i)
  {
    float4 sumV = {0, 0, 0, 0};

    for(int j = 0; j < N; ++j)
    {
      float dx = p1.posW[i].x - p1.posW[j].x;
      float dy = p1.posW[i].y - p1.posW[j].y;
      float dz = p1.posW[i].z - p1.posW[j].z;

      float r = sqrtf(dx*dx + dy*dy + dz*dz);

      float F = -G * dt * p1.posW[j].w / (r * r * r + FLT_MIN);

      float vx, vy, vz;
      if(r > COLLISION_DISTANCE)
      {
        vx = F * dx;
        vy = F * dy;
        vz = F * dz;
      }
      else if(i == j)
      {
        vx = 0.0f;
        vy = 0.0f;
        vz = 0.0f;
      }
      else
      {
        vx = ((p1.posW[i].w * p1.vel[i].x - p1.posW[j].w * p1.vel[i].x + 2 * p1.posW[j].w * p1.vel[j].x) /
            (p1.posW[i].w + p1.posW[j].w)) - p1.vel[i].x;
        vy = ((p1.posW[i].w * p1.vel[i].y - p1.posW[j].w * p1.vel[i].y + 2 * p1.posW[j].w * p1.vel[j].y) /
            (p1.posW[i].w + p1.posW[j].w)) - p1.vel[i].y;
        vz = ((p1.posW[i].w * p1.vel[i].z - p1.posW[j].w * p1.vel[i].z + 2 * p1.posW[j].w * p1.vel[j].z) /
            (p1.posW[i].w + p1.posW[j].w)) - p1.vel[i].z;
      }

      sumV.x += vx;
      sumV.y += vy;
      sumV.z += vz;
    }

    p2.vel[i].x = p1.vel[i].x + sumV.x;
    p2.vel[i].y = p1.vel[i].y + sumV.y;
    p2.vel[i].z = p1.vel[i].z + sumV.z;

    p2.posW[i].x = p1.posW[i].x + p2.vel[i].x * dt;
    p2.posW[i].y = p1.posW[i].y + p2.vel[i].y * dt;
    p2.posW[i].z = p1.posW[i].z + p2.vel[i].z * dt;
  }


}// end of update_particle
//----------------------------------------------------------------------------------------------------------------------


/**
 * CUDA function to add particle to Center of mass
 * @param com      - center of mass
 * @param p        - particle
 */
#pragma acc routine seq
void addParticleToCOM(float4& com, const float4& p)
{
  const float dx = p.x - com.x;
  const float dy = p.y - com.y;
  const float dz = p.z - com.z;

  const float dw = ((p.w + com.w) > 0.0f) ? ( p.w / (p.w + com.w)) : 0.0f;

  com.x += dx * dw;
  com.y += dy * dw;
  com.z += dz * dw;
  com.w += p.w;
}// end of addParticleToCOM
//----------------------------------------------------------------------------------------------------------------------


/// Compute center of gravity
float4 centerOfMassGPU(const Particles& p,
                       float4* tmp,
                       const int        N)
{
  // In first step, compute partial results for each thread
  #pragma acc parallel loop present(p, tmp)
  for(int tid = 0; tid < REDUCTION_THREADS; tid++)
  {
    float4 threadPartialRes = {0, 0, 0, 0};
    int index = tid;
    while(index < N)
    {
      addParticleToCOM(threadPartialRes, p.posW[index]);
      index += REDUCTION_THREADS;
    }
    tmp[tid] = threadPartialRes;
  }

  // In second step, reduce partial results into one global result
  // Synchronization is done on kernel level: every level of reduction
  // is run in separate kernel
  for(int stride = REDUCTION_THREADS / 2; stride > 0; stride >>= 1)
  {
    #pragma acc parallel loop present(p, tmp)
    for(int i = 0; i < stride; i++)
    {
      addParticleToCOM(tmp[i], tmp[i + stride]);
    }
  }

  // Move result to CPU
  #pragma acc update host(tmp[1])

  return tmp[0];

}// end of centerOfMassGPU
//----------------------------------------------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Compute center of mass on CPU
float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting points and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// end of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
