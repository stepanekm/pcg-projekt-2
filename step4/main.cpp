/**
 * @file      main.cpp
 *
 * @author    Martin Štěpánek \n
 *            Faculty of Information Technology \n
 *            Brno University of Technology \n
 *            xstepa59@stud.fit.vutbr.cz
 *
 * @brief     PCG Assignment 2
 *            N-Body simulation in ACC
 *
 * @version   2021
 *
 * @date      11 November  2020, 11:22 (created) \n
 * @date      11 November  2020, 11:37 (revised) \n
 *
 */

#include <chrono>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <type_traits>

#include "nbody.h"
#include "h5Helper.h"

/**
 * Main routine of the project
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv)
{
  // Parse command line parameters
  if (argc != 7)
  {
    printf("Usage: nbody <N> <dt> <steps> <write intesity> <input> <output>\n");
    exit(EXIT_FAILURE);
  }

  const int   N         = std::stoi(argv[1]);
  const float dt        = std::stof(argv[2]);
  const int   steps     = std::stoi(argv[3]);
  const int   writeFreq = (std::stoi(argv[4]) > 0) ? std::stoi(argv[4]) : 0;

  printf("N: %d\n", N);
  printf("dt: %f\n", dt);
  printf("steps: %d\n", steps);

  const size_t recordsNum = (writeFreq > 0) ? (steps + writeFreq - 1) / writeFreq : 0;


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                                         Code to be implemented                                                   //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // 1.  Memory allocation on CPU
  Particles p1(N);
  Particles p2(N);
  // COM counted on GPU. On CPU side, only one float4 is needed - result computed of GPU will be copied there.
  float4 comOnGPU;
  float4* reduction_res = &comOnGPU;
  // On GPU side, reduction_res have to be an array of size REDUCTION_THREADS because it is used for storing
  // temporary results during reduction
  #pragma acc enter data create(reduction_res[REDUCTION_THREADS])

  // 2. Create memory descriptor
  /*
   * Caution! Create only after CPU side allocation
   * parameters:
   *                                    Stride of two               Offset of the first
   *             Data pointer           consecutive elements        element in floats,
   *                                    in floats, not bytes        not bytes
  */
  MemDesc md(
                   reinterpret_cast<float*>(p1.posW),                4,                          0,            // Position in X
                   reinterpret_cast<float*>(p1.posW),                4,                          1,            // Position in Y
                   reinterpret_cast<float*>(p1.posW),                4,                          2,            // Position in Z
                   reinterpret_cast<float*>(p1.vel),                 4,                          0,            // Velocity in X
                   reinterpret_cast<float*>(p1.vel),                 4,                          1,            // Velocity in Y
                   reinterpret_cast<float*>(p1.vel),                 4,                          2,            // Velocity in Z
                   reinterpret_cast<float*>(p1.posW),                4,                          3,            // Weight
                   N,                                                               // Number of particles
                   recordsNum);                                                     // Number of records in output file



  H5Helper h5Helper(argv[5], argv[6], md);

  // Read data
  try
  {
    h5Helper.init();
    h5Helper.readParticleData();
  } catch (const std::exception& e)
  {
    std::cerr<<e.what()<<std::endl;
    return EXIT_FAILURE;
  }

  // 3. Copy data to GPU
  // Copy weights from p1 to p2, and copy both p1 and p2 to GPU
  // (p2 is copied only because of weights; positions and velocities will not be used and will be overriden in first iteration).
  std::memcpy(p2.posW, p1.posW, N * sizeof(float4));
  p1.copyToGPU();
  p2.copyToGPU();
  #pragma acc wait(Stream::Transfers)

  // Start the time
  auto startTime = std::chrono::high_resolution_clock::now();

  // 4. Run the loop - calculate new Particle positions.
  for (int s = 0; s < steps; s++)
  {
    calculate_velocity(p1, p2, N, dt);

    /// In step 4 - fill in the code to store Particle snapshots.
    if (writeFreq > 0 && (s % writeFreq == 0))
    {
      centerOfMassGPU(p1, reduction_res, N);
      p1.copyToCPU();

      #pragma acc wait(Stream::Transfers)
      h5Helper.writeParticleData(s / writeFreq);

      #pragma acc wait(Stream::COM)
      h5Helper.writeCom(comOnGPU.x, comOnGPU.y, comOnGPU.z, comOnGPU.w, s / writeFreq);

    }
    #pragma acc wait(Stream::ComputeVelocity)
    swap(p1, p2);

  }// for s ...

  // 5. In steps 3 and 4 -  Compute center of gravity
  centerOfMassGPU(p1, reduction_res, N);

  // Stop watchclock
  const auto   endTime = std::chrono::high_resolution_clock::now();
  const double time    = (endTime - startTime) / std::chrono::milliseconds(1);
  printf("Time: %f s\n", time / 1000);


  // 5. Copy data from GPU back to CPU.
  // Result is in p1, so there is no need to copy p2 too
  p1.copyToCPU();
  #pragma acc wait(Stream::Transfers)

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /// Calculate center of gravity
  float4 comOnCPU = centerOfMassCPU(md);


  std::cout<<"Center of mass on CPU:"<<std::endl
    << comOnCPU.x <<", "
    << comOnCPU.y <<", "
    << comOnCPU.z <<", "
    << comOnCPU.w
    << std::endl;

  #pragma acc wait(Stream::COM)
  std::cout<<"Center of mass on GPU:"<<std::endl
    << comOnGPU.x <<", "
    << comOnGPU.y <<", "
    << comOnGPU.z <<", "
    << comOnGPU.w
    <<std::endl;

  // Store final positions of the particles into a file
  h5Helper.writeComFinal(comOnGPU.x, comOnGPU.y, comOnGPU.z, comOnGPU.w);
  h5Helper.writeParticleDataFinal();

  #pragma acc exit data delete(reduction_res)

  return EXIT_SUCCESS;
}// end of main
//----------------------------------------------------------------------------------------------------------------------

