/**
 * @file      nbody.cpp
 *
 * @author    Martin Štěpánek \n
 *            Faculty of Information Technology \n
 *            Brno University of Technology \n
 *            xstepa59@stud.fit.vutbr.cz
 *
 * @brief     PCG Assignment 2
 *            N-Body simulation in ACC
 *
 * @version   2021
 *
 * @date      11 November  2020, 11:22 (created) \n
 * @date      11 November  2020, 11:37 (revised) \n
 *
 */

#include <math.h>
#include <cfloat>
#include "nbody.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                       Declare following structs / classes                                          //
//                                  If necessary, add your own classes / routines                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Compute gravitation velocity
void calculate_gravitation_velocity(const Particles& p,
                                    Velocities&      tmp_vel,
                                    const int        N,
                                    const float      dt)
{
  #pragma acc parallel loop vector_length(512) present(p, tmp_vel)
  for(int i = 0; i < N; ++i)
  {
    float4 sumV = {0, 0, 0, 0};

    for(int j = 0; j < N; ++j)
    {
      float dx = p.posW[i].x - p.posW[j].x;
      float dy = p.posW[i].y - p.posW[j].y;
      float dz = p.posW[i].z - p.posW[j].z;

      float r = sqrt(dx*dx + dy*dy + dz*dz);

      float F = -G * p.posW[i].w * p.posW[j].w / (r * r + FLT_MIN);

      float nx = F * dx / (r + FLT_MIN);
      float ny = F * dy / (r + FLT_MIN);
      float nz = F * dz / (r + FLT_MIN);

      float vx = nx * dt / p.posW[i].w;
      float vy = ny * dt / p.posW[i].w;
      float vz = nz * dt / p.posW[i].w;

      sumV.x += (r > COLLISION_DISTANCE) ? vx : 0.0f;
      sumV.y += (r > COLLISION_DISTANCE) ? vy : 0.0f;
      sumV.z += (r > COLLISION_DISTANCE) ? vz : 0.0f;
    }
    tmp_vel.vel[i] = sumV;
  }


}// end of calculate_gravitation_velocity
//----------------------------------------------------------------------------------------------------------------------

void calculate_collision_velocity(const Particles& p,
                                  Velocities&      tmp_vel,
                                  const int        N,
                                  const float      dt)
{

  #pragma acc parallel loop vector_length(512) present(p, tmp_vel)
  for(int i = 0; i < N; ++i)
  {
    float4 sumV = {0, 0, 0, 0};

    for(int j = 0; j < N; ++j)
    {
      float dx = p.posW[i].x - p.posW[j].x;
      float dy = p.posW[i].y - p.posW[j].y;
      float dz = p.posW[i].z - p.posW[j].z;

      float r = sqrt(dx*dx + dy*dy + dz*dz);

      float vx = ((p.posW[i].w * p.vel[i].x - p.posW[j].w * p.vel[i].x + 2 * p.posW[j].w * p.vel[j].x) /
              (p.posW[i].w + p.posW[j].w)) - p.vel[i].x;
      float vy = ((p.posW[i].w * p.vel[i].y - p.posW[j].w * p.vel[i].y + 2 * p.posW[j].w * p.vel[j].y) /
              (p.posW[i].w + p.posW[j].w)) - p.vel[i].y;
      float vz = ((p.posW[i].w * p.vel[i].z - p.posW[j].w * p.vel[i].z + 2 * p.posW[j].w * p.vel[j].z) /
              (p.posW[i].w + p.posW[j].w)) - p.vel[i].z;

      if(i != j && r < COLLISION_DISTANCE){
          sumV.x += vx;
          sumV.y += vy;
          sumV.z += vz;
      }
    }

    tmp_vel.vel[i].x += sumV.x;
    tmp_vel.vel[i].y += sumV.y;
    tmp_vel.vel[i].z += sumV.z;
  }


}// end of calculate_collision_velocity
//----------------------------------------------------------------------------------------------------------------------

/// Update particle position
void update_particle(const Particles& p,
                     Velocities&      tmp_vel,
                     const int        N,
                     const float      dt)
{

  #pragma acc parallel loop vector_length(512) present(p, tmp_vel)
  for(int i = 0; i < N; ++i)
  {
    p.vel[i].x += tmp_vel.vel[i].x;
    p.vel[i].y += tmp_vel.vel[i].y;
    p.vel[i].z += tmp_vel.vel[i].z;

    p.posW[i].x += p.vel[i].x * dt;
    p.posW[i].y += p.vel[i].y * dt;
    p.posW[i].z += p.vel[i].z * dt;
  }


}// end of update_particle
//----------------------------------------------------------------------------------------------------------------------



/// Compute center of gravity
float4 centerOfMassGPU(const Particles& p,
                       const int        N)
{

  return {0.0f, 0.0f, 0.0f, 0.0f};
}// end of centerOfMassGPU
//----------------------------------------------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Compute center of mass on CPU
float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting points and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// end of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
